### Backend task. ###

Built with java/spring-boot as "(micro)service" To run simply execute following: 

java -jar dist/xeneta-backend.jar 

The only requirement that PC should have java 8 installed Then the frontend will be available on following address: 

http://localhost:8080/

I expanded it a bit so apart of OK/OUTLIE charges there is also SUSPICIOS that is 5% wider then OK range.... Also it is possible to mark SUSPICIOS/OUTLIE charges as OK if there is some discuout/Chrismtmas sale or whatever ;)

To build app PC should have java8 and Gradle installed. Use 

gradle bootRun 

to start app in development mode.
PS: All the data is stored in built-in SQL database.