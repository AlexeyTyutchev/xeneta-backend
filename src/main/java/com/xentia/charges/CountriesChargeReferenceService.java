package com.xentia.charges;


import com.xentia.charges.datamodel.Status;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class CountriesChargeReferenceService {

    static class Range {
        private double low;
        private double high;

        Range(double low, double high){
            this.low = low;
            this.high = high;
        }

        public double getLow() {
            return low;
        }

        public double getHigh() {
            return high;
        }

        boolean contains(double number){
            return (number >= low && number <= high);
        }
    }


    private Map<String, Range> references = new HashMap<>();
    private Map<String, Range> suspiciousReferences = new HashMap<>();

    public CountriesChargeReferenceService() {
        references.put("CN", new Range(100, 182));
        references.put("US", new Range(357, 500));
        references.put("HK", new Range(257, 282));

        for (String s : references.keySet()) {
            Range r = references.get(s);
            suspiciousReferences.put(s, new Range(r.low - (r.low / 100 * 5), r.high + (r.high /100 * 5)));
        }
    }

    public Map<String, Range> getReferences() {
        return references;
    }

    public Map<String, Range> getSuspiciousReferences() {
        return suspiciousReferences;
    }

    public Status getStatus(String countryCode, double price) {
        if (!references.containsKey(countryCode)) return Status.OK;
        else {
            Range range = references.get(countryCode);

            if (range.contains(price)) return Status.OK;
            else if (suspiciousReferences.get(countryCode).contains(price)) return Status.SUSPICIOUS;
            else return Status.OUTLIE;
        }
    }
}