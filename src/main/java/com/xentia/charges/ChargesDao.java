package com.xentia.charges;

import com.xentia.charges.datamodel.PortCharge;
import com.xentia.charges.datamodel.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ChargesDao {
    private static final Logger log =  LoggerFactory.getLogger(ChargesDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<PortCharge> rm = (rs, rowNum) -> {
        PortCharge charge = new PortCharge();

        charge.setPortCode(rs.getString("port_code"));
        charge.setCurrency(rs.getString("currency"));
        charge.setSupplierId(rs.getInt("supplier_id"));
        charge.setPrice(rs.getDouble("price"));
        charge.setNormalizedPrice(rs.getDouble("normalized_price"));
        charge.setStatus(Status.valueOf(rs.getString("status")));

        return charge;
    };


    private final String FIND_CHARGES_SQL = "SELECT * FROM terminal_charges";

    public List<PortCharge> findCharges(boolean unsuccessfulOnly, Integer skip, Integer top) {

        String query = FIND_CHARGES_SQL;

        if (unsuccessfulOnly) {
            query += " WHERE status !='" + Status.OK +"'";
        }

        if (skip != null && skip > 0) {
            query += " OFFSET " + skip + " ROWS";
        }

        if (top != null && top > 0) {
            query += " FETCH FIRST  " + top + " ROWS ONLY";
        }

        return jdbcTemplate.query(query, rm);
    }

    public void storeCharge(PortCharge charge) {
        log.info("Storing charge {}...", charge);

        jdbcTemplate.update("INSERT INTO terminal_charges(port_code, supplier_id, currency, price, normalized_price, status) values(?, ?, ?, ?, ?, ?)",
            charge.getPortCode(), charge.getSupplierId(), charge.getCurrency(), charge.getPrice(), charge.getNormalizedPrice(), charge.getStatus().name()
        );
    }

    public void updateChargeStatus(String portCode, Status st) {
        jdbcTemplate.update("UPDATE terminal_charges SET status = ? where port_code = ?",
            st.name(), portCode
        );
    }

    public long countCharges(boolean unsuccessfulOnly) {
        String query = FIND_CHARGES_SQL.replace("*", "count(1)");

        if (unsuccessfulOnly) {
            query += " WHERE status !='" + Status.OK +"'";
        }

        return jdbcTemplate.queryForObject(query,Long.class);
    }

    private final String FIND_CHARGES_STATISTICS_SQL = "SELECT status, count(1) FROM terminal_charges group by status";

    public List<Map<String, Long>> getChargesStats() {
        return jdbcTemplate.query(FIND_CHARGES_STATISTICS_SQL, (rs, rowNum) -> {
            Map<String, Long> res = new HashMap<>();
            res.put(rs.getString(1), rs.getLong(2));
            return res;
        });
    }
}