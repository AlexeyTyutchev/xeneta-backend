package com.xentia.charges;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.LongNode;
import com.xentia.charges.datamodel.PortCharge;
import com.xentia.charges.datamodel.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


@RestController
public class ChargesResource {

    @Autowired
    private ChargesDao chargesDao;

    @Autowired
    private ChargeProcessingService chargeProcessingService;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TypeReference<List<PortCharge>> typeRef = new TypeReference<List<PortCharge>>() {};


    @GetMapping(path = "/api/charges")
    public List<PortCharge> getPortCharges(
        @RequestParam(value = "unsuccessfulOnly") boolean unsuccessfulOnly,
        @RequestParam(value = "skip", required = false) Integer skip,
        @RequestParam(value = "top", required = false) Integer top
    ) {
        return chargesDao.findCharges(unsuccessfulOnly, skip, top);
    }

    @GetMapping(path = "/api/charges/count")
    public JsonNode getPortCharges(
        @RequestParam(value = "unsuccessfulOnly") boolean unsuccessfulOnly
    ) {
        return objectMapper.createObjectNode().set("count", new LongNode(chargesDao.countCharges(unsuccessfulOnly)));
    }

    @GetMapping(path = "/api/charges/statistics")
    public List<Map<String, Long>> getPortCharges() {
        return chargesDao.getChargesStats();
    }

    @PostMapping(path = "/api/charges")
    public void storeCharge(@RequestBody PortCharge charge) {
        chargesDao.storeCharge(chargeProcessingService.processCharge(charge));
    }

    @PostMapping(path = "/api/charges/{portCode}")
    public void changeChargeStatus(@PathVariable String portCode) {
        chargesDao.updateChargeStatus(portCode, Status.OK);
    }

    @PostMapping("/api/charges/bulk")
    public void handleFileUpload(@RequestParam("file") MultipartFile file) throws Exception {
        List<PortCharge> charges = objectMapper.readValue(file.getBytes(), typeRef);
        for (PortCharge charge : charges) {
            storeCharge(charge);
        }
    }
}