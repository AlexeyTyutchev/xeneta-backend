package com.xentia.charges;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.xentia.charges.datamodel.PortCharge;
import com.xentia.charges.datamodel.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


@RestController
public class CountriesResource {

    @Autowired
    private CountriesChargeReferenceService countriesChargeReferenceService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping(path = "/api/reference")
    public ArrayNode getReference() {
        ArrayNode res = objectMapper.createArrayNode();

        for (Map.Entry<String, CountriesChargeReferenceService.Range> r : countriesChargeReferenceService.getReferences().entrySet()) {
            ObjectNode o = objectMapper.createObjectNode();

            o.set("countryCode", new TextNode(r.getKey()));
            o.set("reference", objectMapper.valueToTree(r.getValue()));
            o.set("suspiciousReference", objectMapper.valueToTree(countriesChargeReferenceService.getSuspiciousReferences().get(r.getKey())));

            res.add(o);
        }

        return res;
    }
}