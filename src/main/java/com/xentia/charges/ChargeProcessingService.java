package com.xentia.charges;

import com.xentia.charges.datamodel.PortCharge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ChargeProcessingService {

    private static final Logger log =  LoggerFactory.getLogger(ChargeProcessingService.class);

    @Autowired
    private CountriesChargeReferenceService countriesChargeReferenceService;

    @Autowired
    private CurrencyService currencyService;


    public PortCharge processCharge(PortCharge ch) {
        log.info("Processing port charge {}...", ch);
        PortCharge res = new PortCharge();
        res.setPortCode(ch.getPortCode());
        res.setCurrency(ch.getCurrency());
        res.setSupplierId(ch.getSupplierId());
        res.setPrice(ch.getPrice());

        Double rate = ch.getCurrency().equals("USD") ? 1: currencyService.getRate(ch.getCurrency());
        log.trace("Rate for {} is {}", ch.getCurrency(), rate);

        res.setNormalizedPrice(Math.round(ch.getPrice() / rate * 100) / 100);
        res.setStatus(countriesChargeReferenceService.getStatus(ch.getCountryCode(), res.getNormalizedPrice()));

        return res;
    }

}
