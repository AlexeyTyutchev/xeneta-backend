package com.xentia.charges;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


@Component
public class CurrencyService {

    private static final Logger log =  LoggerFactory.getLogger(CurrencyService.class);

    private Map<String, Double> rates = new HashMap<>();

    public CurrencyService() {
        log.info("Initializing currencyService. Loading currencies info from openexchangerates.org...");

        RestTemplate rt = new RestTemplate();
        ObjectNode res = rt.getForObject("https://openexchangerates.org/api/latest.json?app_id=935e9471c3314831ac708a41f6bf161e", ObjectNode.class);
        parseCurrencies(res);
        log.info("Loaded {} rates", rates.size());
    }

    private void parseCurrencies(ObjectNode res) {
        log.info("Parsing exchange rates...");
        JsonNode ratesNode = res.get("rates");

        for (Iterator<Map.Entry<String, JsonNode>> it = ratesNode.fields(); it.hasNext(); ) {
            Map.Entry<String, JsonNode> next = it.next();
            rates.put(next.getKey(), next.getValue().asDouble());
        }

    }

    public Double getRate(String currencyCode) {
        return rates.get(currencyCode);
    }
}
