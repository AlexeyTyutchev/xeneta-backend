package com.xentia.charges.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PortCharge {

    @JsonProperty("port")
    private String portCode;

    @JsonProperty("supplier_id")
    private Integer supplierId;
    private String currency;

    @JsonProperty("value")
    private double price;
    private double normalizedPrice;
    private Status status;


    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getNormalizedPrice() {
        return normalizedPrice;
    }

    public void setNormalizedPrice(double normalizedPrice) {
        this.normalizedPrice = normalizedPrice;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCountryCode() {
        return portCode.substring(0, 2);
    }

    @Override
    public String toString() {
        return "{portCode:'" + portCode + '\'' +
                ", supplierId:" + supplierId +
                ", currency:'" + currency + '\'' +
                ", price:" + price +
                ", normalizedPrice:" + normalizedPrice +
                ", status:" + status +
                '}';
    }
}
