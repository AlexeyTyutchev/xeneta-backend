package com.xentia.charges.datamodel;

public enum Status {
    OK, SUSPICIOUS, OUTLIE
}
