CREATE TABLE terminal_charges (
  port_code VARCHAR(5),
  supplier_id INT NOT NULL,
  currency VARCHAR(3) NOT NULL,
  price FLOAT,
  normalized_price FLOAT,
  status VARCHAR(10)
--   ,
--   PRIMARY KEY (port_code, supplier_id)
);